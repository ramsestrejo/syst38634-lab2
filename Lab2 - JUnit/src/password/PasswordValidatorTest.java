package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLength() {
		boolean result = PasswordValidator.checkPasswordLength("123jlkdjfkld");
		assertTrue( "Invalid length" , result == true );
	}
	
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		boolean result = PasswordValidator.checkPasswordLength("1234567");
		assertTrue( "Invalid length" , result == false );
	}	

	@Test
	public void testCheckDigits() {
		fail("Not yet implemented");
	}

}
