package time;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith( Parameterized.class)
public class TimeTest {
	
	public String timeToTest;
	public int seconds;
	
	public TimeTest( String dataTime , int seconds ) {
		timeToTest = dataTime;
		this.seconds = seconds;
	}

	@Parameterized.Parameters 
	public static Collection<Object [ ]> loadData( ) {
		Object [ ][ ] data = { { "12:05:05" , 43505 } //, 
							//{ "12:05:05" , 43503 }
							};
		return Arrays.asList(  data );
	}
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}



	@Test
	public void testGetTotalSeconds() {
		int seconds  = Time.getTotalSeconds( this.timeToTest );
		System.out.println( this.timeToTest );
		assertTrue( "The seconds were not calculated propertly " , seconds == this.seconds );
	}	
	
	@Test
	public void testGetMilliSeconds(  ) {
		int millis = Time.getMilliSeconds( "12:05:05:05" );
		System.out.println( millis );
		assertTrue( "The milliseconds were not calculated properly" , millis == 5 );
	}
	
	@Test (expected = NumberFormatException.class )
	public void testGetMilliSecondsException( ) {
		int millis = Time.getMilliSeconds( "12:05:05:5555" );
	}
	
	public void testGetMilliSecondsBoundaryIn(  ) {
		// use 999
	}
	
	public void testGetMilliSecondsBoundaryOut(  ) {
		// use 1000
	}
	
	
//	@Test
//	public void testGetSeconds() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalMinutes() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalHours() {
//		fail("Not yet implemented");
//	}

}
